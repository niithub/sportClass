import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

    menus = [];
    homeUrl = 'home';
    constructor(@Inject('menuService')private menuService) { }
    ngOnInit() {
        this.menus = [{'menuName' : '地图', 'menuUrl': '/product', 'items': []}, {'menuName' : '学生管理', 'menuUrl': '/product', 'items': []}];
        console.log(this.menus);
    }

}
