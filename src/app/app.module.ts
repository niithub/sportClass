import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgZorroAntdModule, NZ_MESSAGE_CONFIG, NZ_I18N, zh_CN} from 'ng-zorro-antd';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import {AppRoutingModule} from './app-routing.module';
import { MenuComponent } from './menu/menu.component';
import {MenuService} from './menu/menu.service';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {CookieModule} from 'ngx-cookie';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {NgxEchartsModule} from 'ngx-echarts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MnFullpageModule} from 'ngx-fullpage';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { MapComponent } from './map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    HomeComponent,
    MenuComponent,
    MapComponent
  ],
  imports: [
      AppRoutingModule,
      BrowserModule,
      FormsModule,
      HttpClientModule,
      BrowserAnimationsModule,
      MnFullpageModule.forRoot(),
      NgZorroAntdModule.forRoot(),
      ReactiveFormsModule,
      NgxEchartsModule,
      NgxQRCodeModule,
      CookieModule.forRoot(),
      FroalaEditorModule.forRoot(),
      FroalaViewModule.forRoot()
  ],
  providers: [
      {provide: 'menuService', useClass: MenuService},
      { provide: NZ_MESSAGE_CONFIG, useValue: {
              nzDuration: 4500,
              nzMaxStack: 20,
              nzPauseOnHover: true,
              nzAnimate: true
          }},
      { provide: NZ_I18N, useValue: zh_CN }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
