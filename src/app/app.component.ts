import {Component, OnInit} from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(private route: Router) {}
    showNav = false;
    userName = '';
    ngOnInit(): void {
        this.checkCurrentUser();
    }

    checkCurrentUser() {
        this.showNav = true;
        this.userName = 'admin';
    }
}
